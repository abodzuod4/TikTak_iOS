//
//  ScanPackage2VC.swift
//  TikTak
//
//  Created by Apple User on 10/29/21.
//

import UIKit

class ScanPackage2VC: UIViewController {

    @IBOutlet weak var ScanPackageBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Scan Package"
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.backButtonTitle = " "

        ScanPackageBtn.addBtnBorder(width: 1, cornerRadius: 20, color: #colorLiteral(red: 0.9415044188, green: 0, blue: 0.07549958676, alpha: 1))

        // Do any additional setup after loading the view.
    }
    
    @IBAction func scanTapped(_ sender: Any) {

        if let nextVC = UIStoryboard(name: "SeeMore", bundle: nil).instantiateViewController(withIdentifier: "CurrentPackageVC") as? CurrentPackageVC {
                self.navigationController?.pushViewController(nextVC, animated: true)
    }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
