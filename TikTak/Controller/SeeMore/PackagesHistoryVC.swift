//
//  PackagesHistoryVC.swift
//  TikTak
//
//  Created by Apple User on 10/30/21.
//

import UIKit

class PackagesHistoryVC: UIViewController {




    @IBOutlet weak var packagesTableview: UITableView!

    var packageArray : [String] = ["card-2.png"]


    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Packages History"
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.backButtonTitle = " "



        setupCollectionVie2()

        // Do any additional setup after loading the view.
    }
    

    func setupCollectionVie2() {
        self.packagesTableview.register(UINib(nibName: "PackagesHistoryCell", bundle: nil), forCellReuseIdentifier: "PackagesHistoryCell")
        self.packagesTableview.delegate = self
        self.packagesTableview.dataSource = self
    }

}



extension PackagesHistoryVC : UITableViewDelegate,UITableViewDataSource  {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }
        if section == 1{
            return 5
        }

        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PackagesHistoryCell", for: indexPath)as? PackagesHistoryCell {
//            cell.packagesImage.image = UIImage(named: packageArray[indexPath.row])

             return cell
    }

    }
        if indexPath.section == 1{
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PackagesHistoryCell", for: indexPath)as? PackagesHistoryCell {
//            cell.packagesImage.image = UIImage(named: packageArray[indexPath.row])


             return cell
    }

    }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 200

        //or whatever you need
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {

        if section == 0{
            return "All Categories"
        }
        if section == 1{
            return "package History"
        }


        return ""
    }

    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView


        header.tintColor = .white
        header.backgroundView?.backgroundColor = #colorLiteral(red: 0.9688121676, green: 0.9688346982, blue: 0.9688225389, alpha: 1)
        header.textLabel?.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//        header.textLabel?.font = UIFont.systemFont(ofSize: 18)
        header.textLabel?.font = .boldSystemFont(ofSize: 16)

    }


    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

        return 50
    }



}
