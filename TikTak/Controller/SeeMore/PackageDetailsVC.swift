//
//  PackageDetailsVC.swift
//  TikTak
//
//  Created by Apple User on 10/29/21.
//

import UIKit

class PackageDetailsVC: UIViewController {

    @IBOutlet weak var subscribeBtn: UIButton!
    @IBOutlet weak var PackagesDetailsCollactionView: UICollectionView!



    var packageArray : [String] = ["card-2.png","card-2.png","card-2.png"]



    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionVie2()
        subscribeBtn.addBtnBorder(width: 1, cornerRadius: 20, color: #colorLiteral(red: 0.9415044188, green: 0, blue: 0.07549958676, alpha: 1))

        self.title = "Ultimate Fun Zone Package"
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.backButtonTitle = " "
        
        // Do any additional setup after loading the view.
    }






    func setupCollectionVie2() {
        self.PackagesDetailsCollactionView.register(UINib(nibName: "PackageDetailsCell", bundle: nil), forCellWithReuseIdentifier: "PackageDetailsCell")
        self.PackagesDetailsCollactionView.delegate = self
        self.PackagesDetailsCollactionView.dataSource = self
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func subscribeTapped(_ sender: Any) {

        if let nextVC = UIStoryboard(name: "SeeMore", bundle: nil).instantiateViewController(withIdentifier: "ScanPackageVC") as? ScanPackageVC {
                self.navigationController?.pushViewController(nextVC, animated: true)
        }
    }
    
}


extension PackageDetailsVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {


        return packageArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PackageDetailsCell", for: indexPath) as? PackageDetailsCell {
            cell.packageDetailsImage.image = UIImage(named: packageArray[indexPath.row])
             return cell
         }
         return UICollectionViewCell()
     }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize{

        return CGSize(width: 100, height: 100
        )

    }
}
