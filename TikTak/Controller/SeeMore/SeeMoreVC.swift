//
//  SeeMore.swift
//  TikTak
//
//  Created by Apple User on 10/22/21.
//

import UIKit

class SeeMoreVC: UIViewController {


    //MARK:- IBOutlet

    @IBOutlet weak var seeMoreCollectionViewContoller: UICollectionView!


    //MARK:- Variable

    var packageArray : [String] = ["card-2.png","card-2.png","card-2.png","card-2.png","card-2.png","card-2.png"]

    //MARK:- ViewDidLoad

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Explore The Package "
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.backButtonTitle = " "



        setupCollectionVie2()
    }


    //MARK:- Funcation

    func setupCollectionVie2() {
        self.seeMoreCollectionViewContoller.register(UINib(nibName: "SeeMoreCell", bundle: nil), forCellWithReuseIdentifier: "SeeMoreCell")
        self.seeMoreCollectionViewContoller.delegate = self
        self.seeMoreCollectionViewContoller.dataSource = self
    }


    //MARK:- IBAction


}

//MARK:- Extension

extension SeeMoreVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {


        return packageArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SeeMoreCell", for: indexPath) as? SeeMoreCell {
            cell.seeMoreImage.image = UIImage(named: packageArray[indexPath.row])
             return cell
         }
         return UICollectionViewCell()
     }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize{


        return CGSize(width: 200, height: 350)

    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if let nextVC = UIStoryboard(name: "SeeMore", bundle: nil).instantiateViewController(withIdentifier: "PackageDetailsVC") as? PackageDetailsVC {
                self.navigationController?.pushViewController(nextVC, animated: true)
        }

    }
    
}
