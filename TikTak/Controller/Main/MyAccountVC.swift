//
//  myAccountVC.swift
//  TikTak
//
//  Created by Apple User on 10/13/21.
//

import UIKit

class MyAccountVC: UIViewController {


    //MARK:-IBOutlet
    
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var personView: UIView!
    @IBOutlet weak var personImage: UIImageView!

    //MARK:-Variables



    //MARK:-viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "My Account"
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.backButtonTitle = " "

        personView.circleDesign()
        editButton.addBtnBorder(width: 1, cornerRadius: 10, color: #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1))

        // Do any additional setup after loading the view.
    }

    //MARK:-funcation




    //MARK:-IBAction
    

    

}
