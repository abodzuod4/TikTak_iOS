//
//  homeVC.swift
//  TikTak
//
//  Created by Apple User on 10/13/21.
//

import UIKit
import ImageSlideshow

class HomeVC: UIViewController {

    //MARK:-IBOutlet
    @IBOutlet weak var PackagesCollectionView: UICollectionView!
    @IBOutlet weak var slideView: ImageSlideshow!{
        didSet{
            slideView.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
            let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
            slideView.addGestureRecognizer(gestureRecognizer)
            slideView.contentMode = .scaleAspectFit
            slideView.contentScaleMode = .scaleAspectFill
            slideView.clipsToBounds = true
            slideView.slideshowInterval = 4
            
        }
    }

    //MARK:-Variables
    var sdWebArray = [SDWebImageSource]()
    var imgArray = ["https://media.istockphoto.com/photos/bowling-picture-id467985831?k=20&m=467985831&s=612x612&w=0&h=EbVpyDIDHZYvimifynCf_V2xQ2I4wCSYPHC1NYp6pQ4=","https://southwycklanes.com/wp-content/uploads/2018/05/pins.jpg","https://southwycklanes.com/wp-content/uploads/2018/05/pins.jpg" ]
    var packageArray : [String] = ["card-1.png","card-2.png","card.png"]
    
    //MARK:-viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Home Page "
        self.navigationController?.navigationBar.isHidden = false

        //        setupTableView()
        for image in imgArray {
            self.sdWebArray.append(SDWebImageSource(urlString: image, placeholder: UIImage(named: "005Homepage"))!)
        }

        slideView.setImageInputs(sdWebArray)
        setupCollectionVie2()
    }
    
    //MARK:- Funtions


    func setupCollectionVie2() {
        self.PackagesCollectionView.register(UINib(nibName: "PackageCell", bundle: nil), forCellWithReuseIdentifier: "PackageCell")
        self.PackagesCollectionView.delegate = self
        self.PackagesCollectionView.dataSource = self
    }

    @objc func didTap() {
        slideView.presentFullScreenController(from: self)
    }
    
    //
    //    func setupTableView() {
    //        tableView.register(UINib(nibName: "BannerCell", bundle: nil), forCellReuseIdentifier: "BannerCell")
    //
    //        tableView.delegate = self
    //        tableView.dataSource = self
    //    }

    //MARK:- IBAction

    @IBAction func seeMoreTapped(_ sender: Any) {


        if let nextVC = UIStoryboard(name: "SeeMore", bundle: nil).instantiateViewController(withIdentifier: "SeeMoreVC") as? SeeMoreVC {
                self.navigationController?.pushViewController(nextVC, animated: true)
        }
    }
    


}


extension HomeVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {


        return packageArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PackageCell", for: indexPath) as? PackageCell {
            cell.packageImage.image = UIImage(named: packageArray[indexPath.row])
             return cell
         }
         return UICollectionViewCell()
     }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize{

        return CGSize(width: 350, height: 200
        )

    }
}



//extension homeVC : UITableViewDataSource, UITableViewDelegate {
//
//
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//
//        return 2
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
//    {
//        if indexPath.row == 0{
//            if let cell = tableView.dequeueReusableCell(withIdentifier: "BannerCell", for: indexPath) as? BannerCell {
//                cell.bannerView.setImageInputs(sdWebArray)
//                cell.Vc = self
//                return cell
//            }
//        }
//
//        return UITableViewCell()
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.row == 0{
//            return 200
//        }
//        return 1
//    }
//
//}
//
//




