//
//  FirstVC.swift
//  TikTak
//
//  Created by Apple User on 10/2/21.
//

import UIKit
import BWWalkthrough

class FirstVC: UIViewController {



    @IBOutlet weak var walkthroughImageView: UIImageView!
    @IBOutlet var walkthroughView: UIView!

    @IBOutlet weak var carouselView: iCarousel!

    //MARK:- Variables
    var walkthroughArray = [WalkthroughModel]()
    var Vc:MasterVC?

    override func viewDidLoad() {
        super.viewDidLoad()

        carouselView.dataSource = self
        carouselView.delegate = self

        carouselView.type = .cylinder

        carouselView.centerItemWhenSelected = true

        carouselView.isPagingEnabled = true

        let mm  = WalkthroughModel(image: UIImage(named: "card")!, title: "Welcome!", body: " Making friends is easy as waving your hand back and forth in easy step.")

        walkthroughArray.append(mm)
        walkthroughArray.append(mm)
        walkthroughArray.append(mm)

        carouselView.reloadData()


    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension FirstVC : iCarouselDelegate , iCarouselDataSource {
    func numberOfItems(in carousel: iCarousel) -> Int {
        return walkthroughArray.count
    }

    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        if carousel.currentItemIndex == 2 {
            let viewTemp1 = carousel.viewWithTag(1000)
            viewTemp1?.alpha = 0.25

            let viewTemp2 = carousel.viewWithTag(1001)
            viewTemp2?.alpha = 0.5

            let viewTemp3 = carousel.viewWithTag(1003)
            viewTemp3?.alpha = 1

        } else if carousel.currentItemIndex == 1 {

            let viewTemp1 = carousel.viewWithTag(1000)
            viewTemp1?.alpha = 0.5

            let viewTemp2 = carousel.viewWithTag(1001)
            viewTemp2?.alpha = 1

            let viewTemp3 = carousel.viewWithTag(1003)
            viewTemp3?.alpha = 1

        } else if carousel.currentItemIndex == 0 {

            let viewTemp1 = carousel.viewWithTag(1000)
            viewTemp1?.alpha = 1

            let viewTemp2 = carousel.viewWithTag(1001)
            viewTemp2?.alpha = 1

            let viewTemp3 = carousel.viewWithTag(1003)
            viewTemp3?.alpha = 1

        }

        if let Vc = self.Vc{
            Vc.pager.currentPage = carousel.currentItemIndex
        }
    }

    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        //walkthroughImageView.image = walkthroughArray[0].image

        let tempView = UIView(frame: CGRect(x: 20, y: 0, width: 400, height: 200))
        let frame = CGRect(x: 20, y: 0, width: 400, height: 400)
        let imageView = UIImageView()
        imageView.frame = frame
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = UIColor(named: "Base Color")
//        imageView.tag = index + 1000
//
        let frame2 = CGRect(x: 100, y: 20, width: 200, height: 250)
        let imageView2 = UIImageView()
        imageView2.frame = frame2
        imageView2.contentMode = .scaleAspectFill
        imageView2.image = walkthroughArray[index].image


        let frame3 = CGRect(x: 90, y: 320, width: 240, height: 21)
        let titleLabel = UILabel(frame: frame3)
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.boldSystemFont(ofSize: 20.0)
        titleLabel.text = walkthroughArray[index].title
        titleLabel.textColor = .red

        let frame4 = CGRect(x: 30, y: 350, width: 300, height: 80)
        let titleLabel2 = UILabel(frame: frame4)
        titleLabel2.textAlignment = .center
        titleLabel2.numberOfLines = 2
        titleLabel2.font = titleLabel2.font.withSize(16)
        titleLabel2.text = walkthroughArray[index].body
        titleLabel2.textColor = .darkText

        tempView.addSubview(imageView)
        tempView.addSubview(imageView2)
        tempView.addSubview(titleLabel)
        tempView.addSubview(titleLabel2)

        return tempView
    }
}
