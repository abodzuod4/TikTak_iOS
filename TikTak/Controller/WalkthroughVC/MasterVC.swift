//
//  MasterVC.swift
//  TikTak
//
//  Created by Apple User on 10/2/21.
//

import UIKit
import BWWalkthrough

class MasterVC: BWWalkthroughViewController,BWWalkthroughViewControllerDelegate   {


    @IBOutlet weak var pager: UIPageControl!


    @IBOutlet weak var logoImg: UIImageView!
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var logInBtn: UIButton!

    

    override func viewDidLoad() {
        super.viewDidLoad()
        signUpBtn.addBtnBorder(width: 1, cornerRadius: 20, color: #colorLiteral(red: 0.9415044188, green: 0, blue: 0.07549958676, alpha: 1))
        logInBtn.addBtnBorder(width: 1, cornerRadius: 20, color: #colorLiteral(red: 0.9415044188, green: 0, blue: 0.07549958676, alpha: 1))

//        btnDesgin()

        self.delegate = self
        self.navigationController?.navigationBar.isHidden = true
        self.navigationItem.backButtonTitle = " "

        if let firstVC = UIStoryboard(name: "Walkthrough", bundle: nil).instantiateViewController(withIdentifier: "FirstVC") as? FirstVC {
            firstVC.Vc = self
            self.add(viewController: firstVC)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true

    }


//    func btnDesgin(){
//        signUpBtn.layer.borderWidth = 1
//        signUpBtn.layer.borderColor = #colorLiteral(red: 0.9415044188, green: 0, blue: 0.07549958676, alpha: 1)
//        logInBtn.layer.borderWidth = 1
//        logInBtn.layer.borderColor = #colorLiteral(red: 0.9415044188, green: 0, blue: 0.07549958676, alpha: 1)
//    }


    //MARK:- Functions
//    static func storyboardInstance() -> MasterVC? {
//        let storyboard = UIStoryboard(name: "Walkthrough", bundle: nil)
//        return storyboard.instantiateViewController(withIdentifier: "MasterVC") as? MasterVC
//    }

    @IBAction func skipTapped(_ sender: Any) {

        if let nextVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarVC") as? TabBarVC {
                self.navigationController?.pushViewController(nextVC, animated: true)
        }
    }



    @IBAction func signUpTapped(_ sender: Any) {

        if let nextVC = UIStoryboard(name: "LoginRegister", bundle: nil).instantiateViewController(withIdentifier: "registerVC") as? registerVC {
                self.navigationController?.pushViewController(nextVC, animated: true)
        }
    }

    @IBAction func logInTapped(_ sender: Any) {

        if let nextVC = UIStoryboard(name: "LoginRegister", bundle: nil).instantiateViewController(withIdentifier: "loginVC") as? loginVC {
                self.navigationController?.pushViewController(nextVC, animated: true)
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

