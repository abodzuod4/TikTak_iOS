//
//  loginVC.swift
//  TikTak
//
//  Created by Apple User on 10/4/21.
//

import UIKit

class loginVC: UIViewController {

    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var passwordView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        vcDesign()

        self.title = "Login"
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.backButtonTitle = " "

        // Do any additional setup after loading the view.
    }

    @IBAction func forgotPassTapped(_ sender: Any) {


        if let nextVC = UIStoryboard(name: "LoginRegister", bundle: nil).instantiateViewController(withIdentifier: "ForgotPassVC") as? ForgotPassVC {
                self.navigationController?.pushViewController(nextVC, animated: true)
        }
        
    }

    func  vcDesign() {
        emailView.addViewBorder(width: 1, cornerRadius: 20, color: #colorLiteral(red: 0.9415044188, green: 0, blue: 0.07549958676, alpha: 1))
        passwordView.addViewBorder(width: 1, cornerRadius: 20, color: #colorLiteral(red: 0.9415044188, green: 0, blue: 0.07549958676, alpha: 1))
        signInBtn.addViewBorder(width: 1, cornerRadius: 20, color: #colorLiteral(red: 0.9415044188, green: 0, blue: 0.07549958676, alpha: 1))
        signUpBtn.addViewBorder(width: 1, cornerRadius: 20, color: #colorLiteral(red: 0.9415044188, green: 0, blue: 0.07549958676, alpha: 1))



    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
