//
//  ForgotPassVC.swift
//  TikTak
//
//  Created by Apple User on 10/9/21.
//

import UIKit

class ForgotPassVC: UIViewController {

    @IBOutlet weak var nextView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var emailTxf: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        vcDesign()
        self.title = "Forgot Password"
        self.navigationController?.navigationBar.isHidden = false
        // Do any additional setup after loading the view.
        self.navigationItem.backButtonTitle = " "
    }

    func vcDesign() {
        nextView.addViewBorder(width: 1, cornerRadius: 20, color: #colorLiteral(red: 0.9415044188, green: 0, blue: 0.07549958676, alpha: 1))
        emailView.addViewBorder(width: 1, cornerRadius: 20, color: #colorLiteral(red: 0.9415044188, green: 0, blue: 0.07549958676, alpha: 1))
        
    }
    
    @IBAction func nextTapped(_ sender: Any) {

        if let nextVC = UIStoryboard(name: "LoginRegister", bundle: nil).instantiateViewController(withIdentifier: "NewPasswordVC") as? NewPasswordVC {
                self.navigationController?.pushViewController(nextVC, animated: true)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
