//
//  NewPasswordVC.swift
//  TikTak
//
//  Created by Apple User on 10/9/21.
//

import UIKit

class NewPasswordVC: UIViewController {

    @IBOutlet weak var nextView: UIView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var confermPassTxf: UIView!
    @IBOutlet weak var passwordTxf: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        vcDesign()
        self.title = "New Password"
        self.navigationController?.navigationBar.isHidden = false
        // Do any additional setup after loading the view.
        self.navigationItem.backButtonTitle = " "
    }

    func vcDesign() {
        confermPassTxf.addViewBorder(width: 1, cornerRadius: 20, color: #colorLiteral(red: 0.9415044188, green: 0, blue: 0.07549958676, alpha: 1))
        passwordTxf.addViewBorder(width: 1, cornerRadius: 20, color: #colorLiteral(red: 0.9415044188, green: 0, blue: 0.07549958676, alpha: 1))
        nextView.addViewBorder(width: 1, cornerRadius: 20, color: #colorLiteral(red: 0.9415044188, green: 0, blue: 0.07549958676, alpha: 1))

    }

    @IBAction func nextTapped(_ sender: Any) {

        if let nextVC = UIStoryboard(name: "LoginRegister", bundle: nil).instantiateViewController(withIdentifier: "SuccessVC") as? SuccessVC {
                self.navigationController?.pushViewController(nextVC, animated: true)
        }
    }
}
