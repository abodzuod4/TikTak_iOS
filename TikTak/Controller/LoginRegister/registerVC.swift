//
//  registerVC.swift
//  TikTak
//
//  Created by Apple User on 10/4/21.
//

import UIKit

class registerVC: UIViewController {

    //MARK: - IBOutlet
    @IBOutlet weak var fullNameView: UIView!
    @IBOutlet weak var emailView: UIView!

    @IBOutlet weak var mobileNumView: UIView!
    @IBOutlet weak var genderView: UIView!
    @IBOutlet weak var birthdayView: UIView!
    @IBOutlet weak var passwordView: UIView!

    //MARK: - Variables


    //MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        vcDesign()
        self.title = "Sign Up"
        self.navigationController?.navigationBar.isHidden = false
//        self.navigationController?.navigationBar.barTintColor = UIColor.red
//        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        self.navigationItem.backButtonTitle = " "
        // Do any additional setup after loading the view.
    }
    //MARK: - Functions
    func vcDesign(){
        fullNameView.addViewBorder(width: 1, cornerRadius: 20, color: #colorLiteral(red: 0.9415044188, green: 0, blue: 0.07549958676, alpha: 1))
        emailView.addViewBorder(width: 1, cornerRadius: 20, color: #colorLiteral(red: 0.9415044188, green: 0, blue: 0.07549958676, alpha: 1))
        mobileNumView.addViewBorder(width: 1, cornerRadius: 20, color: #colorLiteral(red: 0.9415044188, green: 0, blue: 0.07549958676, alpha: 1))
        genderView.addViewBorder(width: 1, cornerRadius: 20, color: #colorLiteral(red: 0.9415044188, green: 0, blue: 0.07549958676, alpha: 1))
        birthdayView.addViewBorder(width: 1, cornerRadius: 20, color: #colorLiteral(red: 0.9415044188, green: 0, blue: 0.07549958676, alpha: 1))
        passwordView.addViewBorder(width: 1, cornerRadius: 20, color: #colorLiteral(red: 0.9415044188, green: 0, blue: 0.07549958676, alpha: 1))

    }

    //MARK: - IBAction


//    func btnDesgin(){
//        signUpBtn.layer.borderWidth = 1
//        signUpBtn.layer.borderColor = #colorLiteral(red: 0.9415044188, green: 0, blue: 0.07549958676, alpha: 1)
//        logInBtn.layer.borderWidth = 1
//        logInBtn.layer.borderColor = #colorLiteral(red: 0.9415044188, green: 0, blue: 0.07549958676, alpha: 1)
//    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}




