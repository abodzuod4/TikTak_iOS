//
//  PackageCell.swift
//  TikTak
//
//  Created by Apple User on 10/18/21.
//

import UIKit

class PackageCell: UICollectionViewCell {

    @IBOutlet weak var viewCollection: UIView!
    @IBOutlet weak var packageImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
