//
//  WalkthroughModel.swift
//  SEF-NEW
//
//  Created by Mohamed Khalil on 6/11/19.
//  Copyright © 2019 Mohamed Khalil. All rights reserved.
//

import Foundation

struct WalkthroughModel {
    var image : UIImage
    var title : String
    var body : String
}
