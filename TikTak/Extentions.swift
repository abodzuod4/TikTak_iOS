//
//  Extentions.swift
//  TikTak
//
//  Created by Apple User on 10/5/21.
//

import Foundation
import UIKit

extension UIButton{
    func addBtnBorder(width:CGFloat, cornerRadius:CGFloat, color:CGColor?){
        self.layer.borderWidth = width
        self.layer.cornerRadius = cornerRadius
        self.layer.borderColor = color

    }
}
extension UIView{
    func addViewBorder(width:CGFloat, cornerRadius:CGFloat, color:CGColor?){
        self.layer.borderWidth = width
        self.layer.cornerRadius = cornerRadius
        self.layer.borderColor = color

    }
}
extension UIView {
func circleDesign (){
    self.layer.cornerRadius = self.frame.size.width/2
    self.layer.borderWidth = 1
    self.layer.borderColor = UIColor.white.cgColor
    self.clipsToBounds = false
}
}

extension UIImageView {
func circleDesign1 (){
    self.layer.cornerRadius = self.frame.size.width/2
    self.layer.borderWidth = 1
    self.layer.borderColor = UIColor.white.cgColor
    self.clipsToBounds = false
}
}
