//
//  ImageSliderCell.swift
//  Giant Group
//
//  Created by Hala Zyod on 10/22/20.
//  Copyright © 2020 QTechNetworks. All rights reserved.
//

import UIKit
import ImageSlideshow
class BannersCell: UITableViewCell {
    var impArray = [SDWebImageSource]()
    var Vc: homeVC?

    @IBOutlet weak var sliderView: ImageSlideshow!{
        didSet{
            sliderView.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
            let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
            sliderView.addGestureRecognizer(gestureRecognizer)
            sliderView.contentMode = .scaleAspectFill
            sliderView.contentScaleMode = .scaleAspectFill
            sliderView.clipsToBounds = true
            sliderView.slideshowInterval = 4

        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    @objc func didTap() {
        guard let Vc = self.Vc else {return}
        sliderView.presentFullScreenController(from: Vc)
    }
}
